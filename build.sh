#!/bin/bash 


SRC=${PWD}/src
PUB=${PWD}/public
BUILD=${PWD}
git submodule update --init --recursive

######################################################################
##																	##
## 				RUN EVERY TIME A PACKAGE IS ADDED		            ##
##																	##
######################################################################

# sudo echo 'non_root ALL=NOPASSWD: ALL' >> /etc/sudoers
# sudo mkdir /home/non_root
# sudo chown -R non_root:non_root /home/non_root

# === UPDATE BINARIES
X86=${PUB}/x86_64

for dir in ${SRC}/*; do
    bash $( cd ${dir} && PKGDEST=${X86} makepkg -cf --noconfirm --log)
    echo "${dir}"
done


# === UPDATE DB
(cd ${X86} && repo-add 'custom.db.tar.gz' *.pkg.tar.zst)

# === UPDATE STATIC SITE
${BUILD}/scripts/static_generator.py ${PUB}
